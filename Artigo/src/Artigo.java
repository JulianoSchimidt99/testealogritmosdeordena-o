import java.time.Duration;
import java.time.Instant;
import java.util.Random;
import java.util.stream.IntStream;

public class Artigo
{
    public static void main(String args[])
    {
        testeUm();
        testeDois();
        testeTres();
    }

    public static void printVetor (int arr[])
    {
        int aux = 0;

        for (int element : arr)
        {
            System.out.println("vetor["+aux+"]: " + element);
            aux++;
        }
    }

    public static void criaCrescente(int arr[])
    {
        for(int i = 0; i < arr.length; i++)
        {
            arr[i] = i;
        }
    }

    public static void criaInverso(int arr[])
    {
        int aux = 0;

        for(int i = arr.length; i > 0; i--)
        {
            arr[aux] = i;
            aux++;
        }
    }

    public static void criaAleatorio(int arr[])
    {
        boolean contem = true;
        Random r = new Random();

        for(int i=0; i<arr.length; i++)
        {
            while(contem)
            {
                int numero = r.nextInt(arr.length) + 1;
                contem = IntStream.of(arr).anyMatch(x -> x == numero);

                if(!contem)
                    arr[i] = numero;

            }

            contem = true;
        }
    }

    public static void testeUm()
    {
        int[] vetor = new int[250];
       // Instant inicio, fim;
        //Duration duration;
        double inicio, fim;
        
        criaCrescente(vetor);

        inicio = System.nanoTime();
        Sorts.insertionSort(vetor);
        fim = System.nanoTime();

        

        System.out.println("Execução dos vetores de " + vetor.length + " posições.\n");
        System.out.println("----------------------------------------------------------------------------------------------------------------------------------------- \n");
        System.out.println("Insertion Sort: \n");
        System.out.println("O vetor preenchido em ordem crescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");

        vetor = new int[250];
        criaInverso(vetor);

        inicio = System.nanoTime();
        Sorts.insertionSort(vetor);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido em ordem decrescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");criaInverso(vetor);

        vetor = new int[250];
        criaAleatorio(vetor);

        inicio = System.nanoTime();
        Sorts.insertionSort(vetor);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido aleatoriamente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.\n");
        System.out.println("----------------------------------------------------------------------------------------------------------------------------------------- \n");criaCrescente(vetor);
        System.out.println("Selection Sort: \n");

        vetor = new int[250];
        criaCrescente(vetor);

        inicio = System.nanoTime();
        Sorts.selectionSort(vetor);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido em ordem crescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");

        vetor = new int[250];
        criaInverso(vetor);

        inicio = System.nanoTime();
        Sorts.selectionSort(vetor);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido em ordem decrescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");criaInverso(vetor);

        vetor = new int[250];
        criaAleatorio(vetor);

        inicio = System.nanoTime();
        Sorts.selectionSort(vetor);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido aleatoriamente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.\n");
        System.out.println("----------------------------------------------------------------------------------------------------------------------------------------- \n");

        System.out.println("Bubble Sort: \n");

        vetor = new int[250];
        criaCrescente(vetor);

        inicio = System.nanoTime();
        Sorts.bubbleSort(vetor);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido em ordem crescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");

        vetor = new int[250];
        criaInverso(vetor);

        inicio = System.nanoTime();
        Sorts.bubbleSort(vetor);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido em ordem decrescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");criaInverso(vetor);

        vetor = new int[250];
        criaAleatorio(vetor);

        inicio = System.nanoTime();
        Sorts.bubbleSort(vetor);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido aleatoriamente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.\n");
        System.out.println("----------------------------------------------------------------------------------------------------------------------------------------- \n");

        System.out.println("Shell Sort: \n");

        vetor = new int[250];
        criaCrescente(vetor);

        inicio = System.nanoTime();
        Sorts.shellSort(vetor);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido em ordem crescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");

        vetor = new int[250];
        criaInverso(vetor);

        inicio = System.nanoTime();
        Sorts.shellSort(vetor);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido em ordem decrescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");criaInverso(vetor);

        vetor = new int[250];
        criaAleatorio(vetor);

        inicio = System.nanoTime();
        Sorts.shellSort(vetor);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido aleatoriamente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.\n");
        System.out.println("----------------------------------------------------------------------------------------------------------------------------------------- \n");

        System.out.println("Merge Sort: \n");

        vetor = new int[250];
        criaCrescente(vetor);

        Sorts.zerarTrocas();
        inicio = System.nanoTime();
        Sorts.mergeSort(vetor, 0, vetor.length-1);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido em ordem crescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");

        vetor = new int[250];
        criaInverso(vetor);

        Sorts.zerarTrocas();
        inicio = System.nanoTime();
        Sorts.mergeSort(vetor, 0, vetor.length-1);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido em ordem decrescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");criaInverso(vetor);

        vetor = new int[250];
        criaAleatorio(vetor);

        Sorts.zerarTrocas();
        inicio = System.nanoTime();
        Sorts.mergeSort(vetor, 0, vetor.length-1);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido aleatoriamente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.\n");
        System.out.println("----------------------------------------------------------------------------------------------------------------------------------------- \n");

        System.out.println("Quick Sort: \n");

        vetor = new int[250];
        criaCrescente(vetor);

        Sorts.zerarTrocas();
        inicio = System.nanoTime();
        Sorts.quickSort(vetor, 0, vetor.length-1);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido em ordem crescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");

        vetor = new int[250];
        criaInverso(vetor);

        Sorts.zerarTrocas();
        inicio = System.nanoTime();
        Sorts.quickSort(vetor, 0, vetor.length-1);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido em ordem decrescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");criaInverso(vetor);

        vetor = new int[250];
        criaAleatorio(vetor);

        Sorts.zerarTrocas();
        inicio = System.nanoTime();
        Sorts.quickSort(vetor, 0, vetor.length-1);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido aleatoriamente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.\n");
        System.out.println("----------------------------------------------------------------------------------------------------------------------------------------- \n");
    }

    public static void testeDois()
    {
        int[] vetor = new int[2500];
       // Instant inicio, fim;
        //Duration duration;
        double inicio, fim;
        
        criaCrescente(vetor);

        inicio = System.nanoTime();
        Sorts.insertionSort(vetor);
        fim = System.nanoTime();

        

        System.out.println("Execução dos vetores de " + vetor.length + " posições.\n");
        System.out.println("----------------------------------------------------------------------------------------------------------------------------------------- \n");
        System.out.println("Insertion Sort: \n");
        System.out.println("O vetor preenchido em ordem crescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");

        vetor = new int[2500];
        criaInverso(vetor);

        inicio = System.nanoTime();
        Sorts.insertionSort(vetor);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido em ordem decrescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");criaInverso(vetor);

        vetor = new int[2500];
        criaAleatorio(vetor);

        inicio = System.nanoTime();
        Sorts.insertionSort(vetor);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido aleatoriamente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.\n");
        System.out.println("----------------------------------------------------------------------------------------------------------------------------------------- \n");criaCrescente(vetor);
        System.out.println("Selection Sort: \n");

        vetor = new int[2500];
        criaCrescente(vetor);

        inicio = System.nanoTime();
        Sorts.selectionSort(vetor);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido em ordem crescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");

        vetor = new int[2500];
        criaInverso(vetor);

        inicio = System.nanoTime();
        Sorts.selectionSort(vetor);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido em ordem decrescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");criaInverso(vetor);

        vetor = new int[2500];
        criaAleatorio(vetor);

        inicio = System.nanoTime();
        Sorts.selectionSort(vetor);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido aleatoriamente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.\n");
        System.out.println("----------------------------------------------------------------------------------------------------------------------------------------- \n");

        System.out.println("Bubble Sort: \n");

        vetor = new int[2500];
        criaCrescente(vetor);

        inicio = System.nanoTime();
        Sorts.bubbleSort(vetor);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido em ordem crescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");

        vetor = new int[2500];
        criaInverso(vetor);

        inicio = System.nanoTime();
        Sorts.bubbleSort(vetor);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido em ordem decrescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");criaInverso(vetor);

        vetor = new int[2500];
        criaAleatorio(vetor);

        inicio = System.nanoTime();
        Sorts.bubbleSort(vetor);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido aleatoriamente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.\n");
        System.out.println("----------------------------------------------------------------------------------------------------------------------------------------- \n");

        System.out.println("Shell Sort: \n");

        vetor = new int[2500];
        criaCrescente(vetor);

        inicio = System.nanoTime();
        Sorts.shellSort(vetor);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido em ordem crescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");

        vetor = new int[2500];
        criaInverso(vetor);

        inicio = System.nanoTime();
        Sorts.shellSort(vetor);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido em ordem decrescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");criaInverso(vetor);

        vetor = new int[2500];
        criaAleatorio(vetor);

        inicio = System.nanoTime();
        Sorts.shellSort(vetor);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido aleatoriamente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.\n");
        System.out.println("----------------------------------------------------------------------------------------------------------------------------------------- \n");

        System.out.println("Merge Sort: \n");

        vetor = new int[2500];
        criaCrescente(vetor);

        Sorts.zerarTrocas();
        inicio = System.nanoTime();
        Sorts.mergeSort(vetor, 0, vetor.length-1);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido em ordem crescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");

        vetor = new int[2500];
        criaInverso(vetor);

        Sorts.zerarTrocas();
        inicio = System.nanoTime();
        Sorts.mergeSort(vetor, 0, vetor.length-1);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido em ordem decrescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");criaInverso(vetor);

        vetor = new int[2500];
        criaAleatorio(vetor);

        Sorts.zerarTrocas();
        inicio = System.nanoTime();
        Sorts.mergeSort(vetor, 0, vetor.length-1);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido aleatoriamente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.\n");
        System.out.println("----------------------------------------------------------------------------------------------------------------------------------------- \n");

        System.out.println("Quick Sort: \n");

        vetor = new int[2500];
        criaCrescente(vetor);

        Sorts.zerarTrocas();
        inicio = System.nanoTime();
        Sorts.quickSort(vetor, 0, vetor.length-1);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido em ordem crescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");

        vetor = new int[2500];
        criaInverso(vetor);

        Sorts.zerarTrocas();
        inicio = System.nanoTime();
        Sorts.quickSort(vetor, 0, vetor.length-1);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido em ordem decrescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");criaInverso(vetor);

        vetor = new int[2500];
        criaAleatorio(vetor);

        Sorts.zerarTrocas();
        inicio = System.nanoTime();
        Sorts.quickSort(vetor, 0, vetor.length-1);
        fim = System.nanoTime();

        

        System.out.println("O vetor preenchido aleatoriamente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.\n");
        System.out.println("----------------------------------------------------------------------------------------------------------------------------------------- \n");
    }

    public static void testeTres()
    {
        int[] vetor = new int[25000];
       // Instant inicio, fim;
        //Duration duration;
        double inicio, fim;

        criaCrescente(vetor);

        inicio = System.nanoTime();
        Sorts.insertionSort(vetor);
        fim = System.nanoTime();



        System.out.println("Execução dos vetores de " + vetor.length + " posições.\n");
        System.out.println("----------------------------------------------------------------------------------------------------------------------------------------- \n");
        System.out.println("Insertion Sort: \n");
        System.out.println("O vetor preenchido em ordem crescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");

        vetor = new int[25000];
        criaInverso(vetor);

        inicio = System.nanoTime();
        Sorts.insertionSort(vetor);
        fim = System.nanoTime();



        System.out.println("O vetor preenchido em ordem decrescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");criaInverso(vetor);

        vetor = new int[25000];
        criaAleatorio(vetor);

        inicio = System.nanoTime();
        Sorts.insertionSort(vetor);
        fim = System.nanoTime();



        System.out.println("O vetor preenchido aleatoriamente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.\n");
        System.out.println("----------------------------------------------------------------------------------------------------------------------------------------- \n");criaCrescente(vetor);
        System.out.println("Selection Sort: \n");

        vetor = new int[25000];
        criaCrescente(vetor);

        inicio = System.nanoTime();
        Sorts.selectionSort(vetor);
        fim = System.nanoTime();



        System.out.println("O vetor preenchido em ordem crescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");

        vetor = new int[25000];
        criaInverso(vetor);

        inicio = System.nanoTime();
        Sorts.selectionSort(vetor);
        fim = System.nanoTime();



        System.out.println("O vetor preenchido em ordem decrescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");criaInverso(vetor);

        vetor = new int[25000];
        criaAleatorio(vetor);

        inicio = System.nanoTime();
        Sorts.selectionSort(vetor);
        fim = System.nanoTime();



        System.out.println("O vetor preenchido aleatoriamente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.\n");
        System.out.println("----------------------------------------------------------------------------------------------------------------------------------------- \n");

        System.out.println("Bubble Sort: \n");

        vetor = new int[25000];
        criaCrescente(vetor);

        inicio = System.nanoTime();
        Sorts.bubbleSort(vetor);
        fim = System.nanoTime();



        System.out.println("O vetor preenchido em ordem crescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");

        vetor = new int[25000];
        criaInverso(vetor);

        inicio = System.nanoTime();
        Sorts.bubbleSort(vetor);
        fim = System.nanoTime();



        System.out.println("O vetor preenchido em ordem decrescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");criaInverso(vetor);

        vetor = new int[25000];
        criaAleatorio(vetor);

        inicio = System.nanoTime();
        Sorts.bubbleSort(vetor);
        fim = System.nanoTime();



        System.out.println("O vetor preenchido aleatoriamente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.\n");
        System.out.println("----------------------------------------------------------------------------------------------------------------------------------------- \n");

        System.out.println("Shell Sort: \n");

        vetor = new int[25000];
        criaCrescente(vetor);

        inicio = System.nanoTime();
        Sorts.shellSort(vetor);
        fim = System.nanoTime();



        System.out.println("O vetor preenchido em ordem crescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");

        vetor = new int[25000];
        criaInverso(vetor);

        inicio = System.nanoTime();
        Sorts.shellSort(vetor);
        fim = System.nanoTime();



        System.out.println("O vetor preenchido em ordem decrescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");criaInverso(vetor);

        vetor = new int[25000];
        criaAleatorio(vetor);

        inicio = System.nanoTime();
        Sorts.shellSort(vetor);
        fim = System.nanoTime();



        System.out.println("O vetor preenchido aleatoriamente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.\n");
        System.out.println("----------------------------------------------------------------------------------------------------------------------------------------- \n");

        System.out.println("Merge Sort: \n");

        vetor = new int[25000];
        criaCrescente(vetor);

        Sorts.zerarTrocas();
        inicio = System.nanoTime();
        Sorts.mergeSort(vetor, 0, vetor.length-1);
        fim = System.nanoTime();



        System.out.println("O vetor preenchido em ordem crescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");

        vetor = new int[25000];
        criaInverso(vetor);

        Sorts.zerarTrocas();
        inicio = System.nanoTime();
        Sorts.mergeSort(vetor, 0, vetor.length-1);
        fim = System.nanoTime();



        System.out.println("O vetor preenchido em ordem decrescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");criaInverso(vetor);

        vetor = new int[25000];
        criaAleatorio(vetor);

        Sorts.zerarTrocas();
        inicio = System.nanoTime();
        Sorts.mergeSort(vetor, 0, vetor.length-1);
        fim = System.nanoTime();



        System.out.println("O vetor preenchido aleatoriamente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.\n");
        System.out.println("----------------------------------------------------------------------------------------------------------------------------------------- \n");

        System.out.println("Quick Sort: \n");

        vetor = new int[25000];
        criaCrescente(vetor);

        Sorts.zerarTrocas();
        inicio = System.nanoTime();
        Sorts.quickSort(vetor, 0, vetor.length-1);
        fim = System.nanoTime();



        System.out.println("O vetor preenchido em ordem crescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");

        vetor = new int[25000];
        criaInverso(vetor);

        Sorts.zerarTrocas();
        inicio = System.nanoTime();
        Sorts.quickSort(vetor, 0, vetor.length-1);
        fim = System.nanoTime();



        System.out.println("O vetor preenchido em ordem decrescente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.");criaInverso(vetor);

        vetor = new int[25000];
        criaAleatorio(vetor);

        Sorts.zerarTrocas();
        inicio = System.nanoTime();
        Sorts.quickSort(vetor, 0, vetor.length-1);
        fim = System.nanoTime();



        System.out.println("O vetor preenchido aleatoriamente executou em: "+ (fim - inicio) +" nanosegundos e realizou "+ Sorts.getTrocas() + "trocas.\n");
        System.out.println("----------------------------------------------------------------------------------------------------------------------------------------- \n");
    }
}
